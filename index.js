//const fs = require('fs');
const path = require('path');

//fsPromises:
const fsPromises = require('fs').promises;
const fileOps = async() => {
    try{
         const data = await fsPromises.readFile(path.join(__dirname, 'files', 'starter.txt'),'utf8');
         console.log(data);
         //unlink : delete a file
         await fsPromises.unlink(path.join(__dirname, 'files', 'starter.txt'));

         await fsPromises.writeFile(path.join(__dirname, 'files', 'promiseWrite.txt'), data);
         await fsPromises.appendFile(path.join(__dirname, 'files', 'promiseWrite.txt'), '\n\n Nice to meet you.');
         await fsPromises.rename(path.join(__dirname, 'files', 'promiseWrite.txt'), path.join(__dirname, 'files', 'promiseComplete.txt'));
      
         const newData = await fsPromises.readFile(path.join(__dirname, 'files', 'promiseComplete.txt'),'utf8');
         console.log(newData);
        } catch (err) {
        console.error(err);
    }
}
fileOps();



/*
//Read File
fs.readFile(path.join(__dirname, 'files', 'starter.txt'), 'utf8',(err, data) => {
    if(err) throw err ;
    console.log(data);
})
*/
// fi 3uuth data.toString() yani fi blast toString() nzyyd 'utf8'


//console.log('Hello....');

//exit on uncaught errors : 
process.on('uncaughtException', err => {
    console.error(`There was an uncaught error: ${err}`);
    process.exit(1);
})

/*
//call back hell 
//Write File
fs.writeFile(path.join(__dirname, 'files', 'reply.txt'),'Nice to meet youu !!', (err) => {
    if(err) throw err ;
    console.log('Write complete');
     //appendFile : added "Yes it is" in reply.txt
    fs.appendFile(path.join(__dirname, 'files', 'reply.txt'),'\n\nYes it is', (err) => {
        if(err) throw err ;
        console.log('Append complete');

        //rename file
        fs.rename(path.join(__dirname, 'files', 'reply.txt'),path.join(__dirname, 'files', 'newReply.txt'), (err) => {
            if(err) throw err ;
            console.log('Rename complete');
        })
    })
})
*/